<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('halaman.register');
    }

    public function kirim(Request $request){
        //dd($request->all());
        $nama1 = $request->namadep;
        $nama2 = $request->namabel;

        return view('halaman.welcome',compact('nama1','nama2'));
    }
}
