@extends('layout.master')

@section('judul1')
<h1>Buat Data Cast Baru!</h1> 
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label for="namee">Nama Cast</label>
      <input type="text" name="nama" class="form-control" id="namee">
    </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label for="umurr">Umur</label>
      <input type="text" name="umur" class="form-control" id="umurr">
    </div>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label for="bioo">Bio</label>
        <input type="textarea" name="bio" class="form-control" id="bioo">
    </div>
      @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection