@extends('layout.master')

@section('judul1')
Halaman Edit Cast {{$cast->nama}}
@endsection

@section('judul2')
Halaman Edit  Cast {{$cast->nama}}
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method("PUT")
    <div class="form-group">
      <label for="namee">Nama Cast</label>
      <input type="text" name="nama" class="form-control" id="namee" value="{{$cast->nama}}">
    </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label for="umurr">Umur</label>
      <input type="text" name="umur" class="form-control" id="umurr" value="{{$cast->umur}}">
    </div>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label for="bioo">Bio</label>
        <input type="textarea" name="bio" class="form-control" id="bioo" value="{{$cast->bio}}">
    </div>
      @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection