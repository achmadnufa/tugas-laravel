@extends('layout.master')

@section('judul')
<h1>Media Online</h1> 
@endsection

@section('content')
<br>
<h3>Sosial Media Developer</h3>
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
<br>
<h3>Benefit Join di Media Online</h3>
<ul type="bullets">
    <li>Mendapatkan motivasi dari sesama para Developer</li>
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>
<br>
<h3>Cara Bergabung ke Media Online</h3>
<ol type="1">
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
    <li>Selesai</li>
</ol>

@endsection
    