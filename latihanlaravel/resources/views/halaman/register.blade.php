@extends('layout.master')

@section('judul')
<h1>Buat Account Baru!</h1> 
@endsection

@section('content')
<h3>Sign Up Form</h3>
    <form action="/welcome" id="signup" method="post">
    @csrf
        <label for="firname">First name:</label><br>
        <input type="text" id="firname" name="namadep"><br>
        <label for="lasname">Last name:</label><br>
        <input type="text" id="lasname" name="namabel"><br>
        <p>Gender:</p>
        <input type="radio" id="mal" name="gender"><label for="mal">Male</label>
        <input type="radio" id="fem" name="gender"><label for="fem">Female</label>
        <input type="radio" id="oth" name="gender"><label for="oth">Other</label>
        <br><br>
        <label for="nationality">Nationality:</label><br><br>
        <select name="national" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="inggris">Inggris</option>
        </select>
        <br><br>
        <p>Language Spoken:</p>
        <input type="checkbox" id="bind" name="bind" value="bahasa"><label for="bind">Bahasa Indonesia</label><br>
        <input type="checkbox" id="eng" name="eng" value="english"><label for="eng">English</label><br>
        <input type="checkbox" id="othe" name="othe" value="other"><label for="othe">Other</label><br>
        <br>
        <label for="bioo">Bio:</label><br>
        <textarea name="bio" id="bioo" cols="50" rows="10"></textarea>
        <br>
        <button type="submit" form="signup" value="Submit">Submit</button>

    </form>

@endsection    
    